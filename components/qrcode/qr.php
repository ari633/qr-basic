<?php
/**
 * Created by PhpStorm.
 * User: ari
 * Date: 2/19/15
 * Time: 2:02 AM
 */

namespace app\components\qrcode;

include "qrlib.php";

class qr {

    public $input='';
    public $tmpDir;

    public function genereate()
    {
        $filename = hash("sha256", time().''.$this->input);

        \QRcode::png($this->input, $this->tmpDir.$filename.'.png', QR_ECLEVEL_L, 4);

        return $filename;
    }

}