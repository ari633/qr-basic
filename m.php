<?

define("ADMINMODULE",1);
include "../global.inc";   

if(!is_admin($$adm_username_cookie,$adm_pass)) {
  Header("Location:".$prefix."login_admin.phtml".$etc);
  exit;
}   

      
include "../admin/show_function.inc";
include "../admin/pay_method.inc";
include "../inc/js_color.inc";
include "../inc/tab.inc";

$head[css]="";
echo Head($head);
echo option_list();

?>
<script>
function expand(obj){ obj.rows=obj.rows*2; }
function collapse(obj){ if( obj.rows < 5 ) return ;obj.rows=obj.rows/2;}
function ToggleDisplay(val,id){
		if (val == 'C')
			id.style.display = 'block'
		else
			id.style.display = 'none'
}
function ToggleBlock(id){
       if(id.style.display=='block'){
        id.style.display='none';
       }else{
        id.style.display='block';
       }
}
</script>
<style>
a.tag {background:#DBD9C0;padding:1px 5px;
       margin:2px;float:left;
       font-size:13px;font-family:arial;color:#666666;
       text-decoration:none;
       border:1px solid #777777;
       overflow:hidden;
white-space:nowrap;
      }
a.tag:hover {background:#b4b463;color:yellow;font-size:13px;}
a.hit {background:#b4b463;color:#ffffff;font-size:13px;}

.inner_content {width:600px; clear:both; overflow:hidden;}
.inner_content { 
   -moz-border-radius-topright: 10px; border-top-right-radius: 10px; 
   -moz-border-radius-bottomright: 10px; border-bottom-right-radius: 10px;
   -moz-border-radius-topleft: 10px; border-top-left-radius: 10px;
   -moz-border-radius-bottomleft: 10px; border-bottom-left-radius: 10px;    
}
</style>
<?
 


 $sna="<font face=Arial size=2>Single quote ( <font color=red>'</font> ) is not allowed.";

 $p[bgcolor] ="#DBD9C0";
 $p[bgcolor2]="#DBD9C0";
 $p[border2] ="#000099";

 $bgcolor="#5A9C3B";	//"#7BBC3B";
 $subbg="#C9C88E";//"#edfdff";

 $titles  =array();
 $contents=array();


 $A=array("site_info"         =>array("title"=>$l['site_info']          ,"file"=>"site_info.inc"),
          "meta_tag"          =>array("title"=>"Meta Tags"              ,"file"=>"meta_tag.inc"),
          "pay_method"        =>array("title"=>$l['pay_method']         ,"file"=>"pay_method.inc"),               
//          "shipping_fee_count"=>array("title"=>$l['shipping_fee_count'] ,"file"=>"shipping_fee_count.inc"),
          "clp_count"         =>array("title"=>$l['clp_count']          ,"file"=>"clp_count.inc"),
          "admin_username"    =>array("title"=>$l['admin_setting']     ,"file"=>"admin_username.inc"),
          "layout_system"     =>array("title"=>$l['layout_system']      ,"file"=>"layout_system.inc"),
          "layout_settings"   =>array("title"=>$l['layout_settings']    ,"file"=>"layout_settings.inc"),
          "layout_template"   =>array("title"=>$l['layout_template']    ,"file"=>"layout_template.inc"),
//          "rfq"               =>array("title"=>$l['rfq']                ,"file"=>"rfq.inc"),
          "misc"              =>array("title"=>"Misc"                   ,"file"=>"misc.inc")
        );                   

 if (module_access_permission("Watermark")){
  $A['Watermark'] = array("title"=>$l['watermark'], "file"=>"watermark.inc");
 }

 $type=( isset($A[$type]) )?$type:"site_info"; 

 echo "<br /><center>";

 echo "<div class=inner_content>"; 
 while( list($t,$a)=each($A) ){
     if( module_access_permission( "Lite" ) && ($t=="pay_method" || $t=="clp_count" || $t=="layout_system" || $t=="watermark" ) ){
        //No Payment Method, No Credit Point, 
     } else {
        echo "<a href='m.php?type=$t' class='tag".($t==$type?" hit":"")."' > $a[title] </a>";  
     }
 }
 echo "<div style='clear:both;'></div>";
 echo "</div>";

 echo "<form name=adminlayout action=manage.php method=post>";
 echo "<table border=0 cellpadding=0 cellspacing=0 width=600px align=center><tr><td>";
 echo form_hidden("domain",$domain);
 echo form_hidden("pgid",$pgid);

 include $A[$type]['file'];

 echo  implode("",$contents);

 echo "<center><br>".form_submit("$l[admin_save]","class=button");
 echo form_hidden("type",$type );
 echo "</td></tr></table>";
 echo "</form>";
 echo Foot($head);


function resize($id){
 return "<a href='javascript:void(0)' onClick='collapse($id)' title='collapse'><img src='../rule/pic/up_icon.gif' border=0></a>&nbsp;<a href='javascript:void(0)' onClick='expand($id)' title='expand'><img src='../rule/pic/down_icon.gif' border=0></a>";
}
?>