<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">

        <?php
        if(!empty($filename)){
            echo Html::img('@web/assets/qrcode/'.$filename.'.png', ['style'=>'width: 200px;']);
        }
        ?>

        <h1>Generate QRcode!</h1>
        <?= Html::beginForm(['site/index'], 'post', ['enctype' => 'multipart/form-data', 'class'=>'form']) ?>

        <?=  Html::textarea('freetext', '', ['class'=>'form-control', 'placeholder'=>'Enter text to share here', 'required'=>'']) ?>
        <br/>
        <p>
            <?= Html::submitButton('Generate!', ['class'=>'btn btn-primary'])?>
        </p>

        <?= Html::endForm();?>
    </div>

</div>
<div class="row">
    <div class="col-md-12">
        <p>
        <h4>Content example:</h4>
        Phone Number: <code>tel: (049)012-345-678</code><br/>
        SMS App: <code>sms: (049)012-345-678</code><br/>
        Mail To: <code>mailto: mail@aripratomo.info</code><br/>
        </p>
    </div>
</div>
