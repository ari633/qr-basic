<?php
/**
 * Created by PhpStorm.
 * User: ari
 * Date: 2/19/15
 * Time: 12:26 AM
 */

namespace app\models;


use yii\base\Model;
use Yii;

class SocialLogin extends Model{

    public $profile_id;
    public $email;

    private $_user = false;

    public function rules()
    {
        return [
            ['email', 'validateEmail']
        ];
    }

    /**
     * Validates the email.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateEmail($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user) {
                $this->addError($attribute, 'Email not yet registered');
            }
        }
    }

    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }


}