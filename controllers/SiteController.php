<?php

namespace app\controllers;

use app\models\RegisterForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\SocialLogin;
use app\models\ContactForm;
use yii\helpers\Html;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'index'],
                'rules' => [
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'successCallback'],
            ],
        ];
    }

    public function actionIndex()
    {

        $filename = null;
        if(Yii::$app->request->isPost && Yii::$app->request->post('freetext') !='') {
            $qr = new \app\components\qrcode\qr();
            $qr->input = Yii::$app->request->post('freetext');
            $qr->tmpDir = 'assets/qrcode/';
            $filename = $qr->genereate();
        }

        return $this->render('index', ['filename'=>$filename]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionRegister()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new RegisterForm();

        if($model->load(Yii::$app->request->post()))
        {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }else{
            return $this->render('register', [
                'model' => $model,
            ]);
        }

    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }



    public function successCallback($client)
    {
        $attr = $client->getUserAttributes();
        $letters = 'abcefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';

        $client = new \yii\authclient\clients\Facebook();

        if($client->getAccessToken()) {

            $attr = $client->getUserAttributes();
            $model = new SocialLogin();
            $model->email = $attr['email'];

            if ($model->login()) {
                echo "Loggedin";
            } else {
                //register new member
                $register = new RegisterForm();
                $register->email = $attr['email'];
                $register->username = substr(str_shuffle($letters), 0, 8);
                $register->password = substr(str_shuffle($letters), 0, 8);
                $register->signup();

                $login = new SocialLogin();
                $login->email = $attr['email'];
                $login->login();
            }

        }
    }

}
